from selenium.webdriver.common.keys import Keys

from pageObjects.HorizontalSlider import HorizontalSlider


class Test_004_HorizontalSlider:
    url = "https://the-internet.herokuapp.com/horizontal_slider"

    def test_horizontal_slider(self, setup):
        self.driver = setup
        self.driver.get(self.url)

        self.hs = HorizontalSlider(self.driver)

        assert self.hs.indicator.text == "0"

        for _ in range(10):
            self.hs.slider.send_keys(Keys.RIGHT)
        assert self.hs.indicator.text == "5"

        self.driver.close()
