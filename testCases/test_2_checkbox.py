from pageObjects.Checkboxes import Checkbox


class Test_002_SelectCheckbox:
    url = "https://the-internet.herokuapp.com/checkboxes"

    def test_select_checkbox(self, setup):
        self.driver = setup

        self.driver.get(self.url)
        cb = Checkbox(self.driver)

        assert not cb.check_box_1.is_selected()
        assert cb.check_box_2.is_selected()

        cb.check_box_1.click()
        cb.check_box_2.click()

        assert cb.check_box_1.is_selected()
        assert not cb.check_box_2.is_selected()
        self.driver.close()
