from pageObjects.DropdownList import DropdownList


class Test_007_Dropdown:
    url = "https://the-internet.herokuapp.com/dropdown"
    option1 = "Option 1"
    option2 = "Option 2"

    def test_add_remove_element(self, setup):
        self.driver = setup

        self.driver.get(self.url)
        self.dropdown = DropdownList(self.driver)

        for opt in self.dropdown.s.options:
            self.dropdown.s.select_by_visible_text(self.option2)

        self.driver.close()



