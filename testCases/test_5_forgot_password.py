from pageObjects.ForgotPassword import ForgotPassword


class Test_001_ForgotPassword:
    url = "https://the-internet.herokuapp.com/forgot_password"
    usr_email = "xxx@fake.com"
    msg = "Your e-mail's been sent!"

    def test_forgot_password_correct_email(self, setup):
        self.driver = setup

        self.driver.get(self.url)

        retreive = ForgotPassword(self.driver)
        retreive.input_email(self.usr_email)
        retreive.retreive_btn_click()

        assert (self.msg in self.driver.page_source)

        self.driver.close()