from selenium.webdriver.common.by import By


class HorizontalSlider:

    def __init__(self, driver):
        self.driver = driver
        self.slider = driver.find_element(By.TAG_NAME, 'input')
        self.indicator = driver.find_element(By.XPATH, "//span[@id='range']")

    def increase_slider(self):
        pass

    def decrease_slider(self):
        pass
