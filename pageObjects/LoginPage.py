class Login:
    txt_username_xpath = "//input[@id='username']"
    txt_password_xpath = "//input[@id='password']"
    btn_login_xpath = "//i[contains(text(),'Login')]"
    btn_logout_xpath = "//i[contains(text(),'Logout')]"

    def __init__(self, driver):
        self.driver = driver

    def set_username(self, username):
        self.driver.find_element_by_xpath(self.txt_username_xpath).clear()
        self.driver.find_element_by_xpath(self.txt_username_xpath).send_keys(username)

    def set_password(self, password):
        self.driver.find_element_by_xpath(self.txt_password_xpath).clear()
        self.driver.find_element_by_xpath(self.txt_password_xpath).send_keys(password)

    def click_login(self):
        self.driver.find_element_by_xpath(self.btn_login_xpath).click()

    def click_logout(self):
        self.driver.find_element_by_xpath(self.btn_logout_xpath).click()
