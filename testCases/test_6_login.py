from pageObjects.LoginPage import Login


class Test_006_LoginPage:
    url = "https://the-internet.herokuapp.com/login"
    username = "tomsmith"
    password = "SuperSecretPassword!"
    success_msg = "You logged into a secure area!"
    logout_msg = "You logged out of the secure area!"
    wrong_name = "tom"
    wrong_pass = "Super"
    error_msg = "Your username is invalid!"

    def test_login_fail(self, setup):
        self.driver = setup
        self.driver.get(self.url)
        login = Login(self.driver)
        login.set_username(self.wrong_name)
        login.set_password(self.wrong_pass)
        login.click_login()

        assert (self.error_msg in self.driver.page_source)

        self.driver.close()

    def test_login_success(self, setup):
        self.driver = setup
        self.driver.get(self.url)
        login = Login(self.driver)
        login.set_username(self.username)
        login.set_password(self.password)
        login.click_login()

        assert (self.success_msg in self.driver.page_source)

        login.click_logout()

        assert (self.logout_msg in self.driver.page_source)
        self.driver.close()
