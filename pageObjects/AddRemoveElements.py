class AddRemoveElements:
    btn_add_element_xpath = "// button[contains(text(), 'Add Element')]"
    btn_delete_xpath = "//button[contains(text(),'Delete')]"

    def __init__(self, driver):
        self.driver = driver

    def click_add_element_button(self, multiply):
        for x in range(multiply):
            self.driver.find_element_by_xpath(self.btn_add_element_xpath).click()

    def delete_buttons_list(self):
        btn_list = self.driver.find_elements_by_xpath(self.btn_delete_xpath)
        return btn_list

    def click_delete_button(self, multiply):
        for x in range(multiply):
            self.driver.find_element_by_xpath(self.btn_delete_xpath).click()