from selenium.webdriver.common.by import By


class Test_003_BasicAuthorisation:
    user_name = "admin"
    password = "admin"
    wanted_text = "Congratulations! You must have the proper credentials."

    def test_basic_authorisation(self, setup):
        self.driver = setup

        self.driver.get(f"https://{self.user_name}:{self.password}@the-internet.herokuapp.com/basic_auth")

        assert self.driver.find_element(By.XPATH, f"//p [contains(text(), {self.wanted_text})]")
        self.driver.close()
