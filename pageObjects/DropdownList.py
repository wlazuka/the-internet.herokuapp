from selenium.webdriver.support.select import Select


class DropdownList:
    select_dropdown_xpath = "//select[@id='dropdown']"
    selected_option_css = "option[selected]"

    def __init__(self, driver):
        self.driver = driver
        self.s = Select(self.driver.find_element_by_xpath(self.select_dropdown_xpath))
        self.selected = self.driver.find_element_by_css_selector(self.selected_option_css)
