from selenium.webdriver.common.by import By


class Checkbox:

    def __init__(self, driver):
        self.driver = driver
        self.check_box_1 = driver.find_element(By.XPATH, "//input[1]")
        self.check_box_2 = driver.find_element(By.XPATH, "//input[2]")
