class ForgotPassword:
    txt_email_xpath = "//input[@id='email']"
    btn_retrieve_xpath = "//i[contains(text(),'Retrieve password')]"
    div_content_xpath = "//div[@id='content']"

    def __init__(self, driver):
        self.driver = driver
        self.content = driver.find_element_by_xpath(self.div_content_xpath)

    def input_email(self, email):
        email_input = self.driver.find_element_by_xpath(self.txt_email_xpath)
        email_input.clear()
        email_input.send_keys(email)

    def retreive_btn_click(self):
        self.driver.find_element_by_xpath(self.btn_retrieve_xpath).click()


