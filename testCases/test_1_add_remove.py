from pageObjects.AddRemoveElements import AddRemoveElements


class Test_001_AddRemoveElements:
    url = "https://the-internet.herokuapp.com/add_remove_elements/"

    def test_add_remove_element(self, setup):
        self.driver = setup

        self.driver.get(self.url)
        self.add_remove_page = AddRemoveElements(self.driver)

        self.add_remove_page.click_add_element_button(3)

        assert len(self.add_remove_page.delete_buttons_list()) == 3

        for btn in self.add_remove_page.delete_buttons_list():
            btn.click()

        assert not self.add_remove_page.delete_buttons_list()

        self.driver.close()
